// Load required packages
var User = require('../models/user');
var jwt = require('jsonwebtoken');

// Create endpoint /api/users for POST
exports.postUsers = function (req, res) {
    var user = new User({
        channelId: req.body.channelId,
        password: req.body.password,
        email: req.body.email
    });

    user.save(function (err) {
        var result = {};

        if (err) {
            result["status"] = false;
            result["message"] = "Channel Id already exist !";
            result["data"] = err;

            res.json(result);
        } else {

            var token = jwt.sign(user, 'PROMANAGERSECRETKEY', {
                expiresIn: 60 * 60 * 720 // expires in 24 hours
            });

            result["status"] = true;
            result["message"] = "User added in O Auth server !";
            result["data"] = user;
            result["token"] = token;

            res.json(result);
        }
    });
};


/**
 * Signin after passport authentication
 */

exports.logIn = function (req, res) {

    var result = {};

    User.findOne({channelId:req.body.channelId}).exec(function (err, user) {
        
        if(user===null){
            result["status"] = false;
            result["message"] = "Can not find user !";               

            res.json(result);
        }
        else{
               
        user.verifyPassword(req.body.password, function (err, isMatch) {
            if (!isMatch || err) {
                result["status"] = false;
                result["message"] = "Authentication failed !";               

                res.json(result);
            } else {
                var token = jwt.sign(user, 'PROMANAGERSECRETKEY', {
                    expiresIn: 60 * 60 * 720 // expires in 24 hours
                });

                result["status"] = true;
                result["message"] = "Succesfully logged In";
                result["data"] = user;
                result["token"] = token;

                res.json(result);
            }
        });
      }
    });    
};


// Create endpoint /api/users for GET
exports.getUsers = function (req, res) {

    var result = {};

    User.find(function (err, users) {
        if (err)
            return res.send(err);

        result["status"] = true;
        result["message"] = "User list.";
        result["data"] = users;

        res.json(result);
    });
};


/**
 * Token middleware
 */
exports.checkToken = function (req, res, next) {

    var token = req.headers.token || req.body.token || req.query.token || req.headers['x-access-token'];
    // return res.json({success: false, message: 'Failed to authenticate token.'});

    if (req.method === 'OPTIONS') {
        next();
    }
    // decode token
    else if (token) {

        // verifies secret and checks exp
        jwt.verify(token, 'PROMANAGERSECRETKEY', function (err, decoded) {
            if (err) {
                return res.json({status: "error", message: 'Failed to authenticate token.'});
            } else {
                // if everything is good, save to request for use in other routes
                req.decoded = decoded;
                next();
            }
        });

    } else {

        // if there is no token
        // return an error
        return res.status(401).send({
            status: "error",
            message: 'No token provided.'
        });

    }

};