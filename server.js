// Load required packages
var express = require('express');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var session = require('express-session');
var passport = require('passport');

var userController = require('./controllers/user');

// Connect to the beerlocker MongoDB
mongoose.connect('mongodb://localhost:27017/oauth');

// Create our Express application
var app = express();

// Use the body-parser package in our application
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(session({
  secret: 'PROMANAGER SECRET',
  saveUninitialized: true,
  resave: true
}));

// Use the passport package in our application
app.use(passport.initialize());

// Create our Express router
var router = express.Router();


router.route("/users").get(userController.checkToken);

// Create endpoint handlers for /auth
router.route('/auth')  
  .post(userController.postUsers);
  
router.route('/login')  
  .post(userController.logIn);
  
// Create endpoint handlers for /users
router.route('/users')  
  .get(userController.getUsers);
     
// Register all our routes with /api
app.use('/api', router);

// Start the server
app.listen(3000);